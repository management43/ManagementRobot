import json
import os


def get_user_list(config, key):
    with open("{}/Management/{}".format(os.getcwd(), config), "r") as json_file:
        return json.load(json_file)[key]


class Config(object):
    LOGGER = True

    API_ID = 5618399  # integer value, dont use ""
    API_HASH = "372f9b12937f0c2a9f0dcec966add011"
    TOKEN = "2063083084:AAExfu_MAEXXPpTt_pJ_npEH0LdOvZsc8Fw"  # This var used to be API_KEY but it is now TOKEN, adjust accordingly.
    OWNER_ID = 412094015  # If you dont know, run the bot and do /id in your private chat with it, also an integer
    OWNER_USERNAME = "mkspali"
    SUPPORT_CHAT = "TeamAvengerSupport"  # Your own group for support, do not add the @
    JOIN_LOGGER = (
        -1001621742945
    )  # Prints any new group the bot is added to, prints just the name and ID.
    GBAN_LOGS = (
        -1001778968360
    )
    EVENT_LOGS = (
        -1001621742945
    )  # Prints information like gbans, sudo promotes, AI enabled disable states that may help in debugging and shit

    # RECOMMENDED
    DATABASE_URL = "postgres://rcmyksvn:yj0fxrrHGp25ME-fb2ogRiLnLI958EB7@jelani.db.elephantsql.com/rcmyksvn"  # needed for any database modules
    FED_DB_URL = "postgres://qwjgaqra:gBgX3PnSW_OnuudA9YMA-7ZX_3Z0YMpz@tuffi.db.elephantsql.com/qwjgaqra"  # needed for any database modules
    NMODEDATABASE_URL = "postgres://sybqgqra:9cnqEJv7N1q30ZS6d4l7JdcjUj_d3-mO@kesavan.db.elephantsql.com/sybqgqra"  # needed for any database modules
    NOTES_DB_URL = "postgres://nastfdke:6aC29y5E6zRYndiNv_ciPcI2XIRl3jZ6@kesavan.db.elephantsql.com/nastfdke"  # needed for any database modules
    FSUB_DB_URL = "postgres://ncufngfg:S-0RobKEFLK43p45-whHKKWIcWjlp4HR@jelani.db.elephantsql.com/ncufngfg"
    FILTER_DB_URL = "postgres://qmxerruy:RpcxknsWOzmdYo5kODSfMEJfiSJanh37@jelani.db.elephantsql.com/qmxerruy"
    WELCOME_DB_URL = "postgres://xlnoxzuc:kXQAc-pMucfQZmtMvQiWFTFwgXAyzh5P@jelani.db.elephantsql.com/xlnoxzuc"
    LOAD = []
    NO_LOAD = ["rss"]
    WEBHOOK = False
    INFOPIC = True
    ALLOW_CHATS = True
    URL = None
    MONGO_DB = "Avenger"
    MONGO_URI = "mongodb+srv://AvengerBot:Mks#1#1#1@avengerbot.uj63i.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
    MONGO_PORT = "27017"
    BOT_USERNAME = "AvengerBot"
    REDIS_URL = "redis://:44A1zG6fobiIvbCofrbSMQbgSjjikZ0S@redis-15336.c10.us-east-1-4.ec2.cloud.redislabs.com:15336"
    REM_BG_API_KEY = "vZThPiV3b5TbV7gHAFwRt1j3"
    TEMP_DOWNLOAD_DIRECTORY = "./"
    ARQ_API_KEY = "ZFTSCM-BEWVPP-MEUBSZ-BJEHCZ-ARQ"
    ARQ_API_URL = "http://arq.hamker.dev"
    SPAMWATCH_API = "4tA8_mJUxa2s2wsffaN_xhzy1bDNFAFkVfvBLBCCfEAkS2VDeQhgadgo28RiAS6i"  # go to support.spamwat.ch to get key
    SPAMWATCH_SUPPORT_CHAT = "@SpamWatchSupport"

    # OPTIONAL
    ##List of id's -  (not usernames) for users which have sudo access to the bot.
    DRAGONS = get_user_list("elevated_users.json", "sudos")
    ##List of id's - (not usernames) for developers who will have the same perms as the owner
    DEV_USERS = get_user_list("elevated_users.json", "devs")
    ##List of id's (not usernames) for users which are allowed to gban, but can also be banned.
    DEMONS = get_user_list("elevated_users.json", "supports")
    # List of id's (not usernames) for users which WONT be banned/kicked by the bot.
    TIGERS = get_user_list("elevated_users.json", "tigers")
    WOLVES = get_user_list("elevated_users.json", "whitelists")
    DONATION_LINK = None  # EG, paypal
    CERT_PATH = None
    PORT = 5000
    DEL_CMDS = True  # Delete commands that users dont have access to, like delete /ban if a non admin uses it.
    STRICT_GBAN = True
    WORKERS = (
        8  # Number of subthreads to use. Set as number of threads your processor uses
    )
    BAN_STICKER = ""  # banhammer marie sticker id, the bot will send this sticker before banning or kicking a user in chat.
    ALLOW_EXCL = True  # Allow ! commands as well as / (Leave this to true so that blacklist can work)
    CASH_API_KEY = (
        "OT6XX0IXFWMD9J9J"  # Get your API key from https://www.alphavantage.co/support/#api-key
    )
    TIME_API_KEY = "M4RKSVS60811"  # Get your API key from https://timezonedb.com/api
    WALL_API = (
        "awoo"  # For wallpapers, get one from https://wall.alphacoders.com/api.php
    )
    AI_API_KEY = "awoo"  # For chatbot, get one from https://coffeehouse.intellivoid.net/dashboard
    BL_CHATS = []  # List of groups that you want blacklisted.
    SPAMMERS = None


class Production(Config):
    LOGGER = True


class Development(Config):
    LOGGER = True
