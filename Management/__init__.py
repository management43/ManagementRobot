import asyncio
import logging
import os
import sys
import json
import asyncio
import time
import spamwatch
import telegram.ext as tg

from inspect import getfullargspec
from aiohttp import ClientSession
from Python_ARQ import ARQ
from redis import StrictRedis
from telethon import TelegramClient
from telethon.sessions import StringSession
from telethon.sessions import MemorySession
from pyrogram.types import Message
from pyrogram import Client, errors
from pyrogram.errors.exceptions.bad_request_400 import PeerIdInvalid, ChannelInvalid
from pyrogram.types import Chat, User
from ptbcontrib.postgres_persistence import PostgresPersistence
from motor.motor_asyncio import AsyncIOMotorClient as MongoClient

StartTime = time.time()


def get_user_list(__init__, key):
    with open("{}/Management/{}".format(os.getcwd(), __init__), "r") as json_file:
        return json.load(json_file)[key]


FORMAT = "[MANAGEMENT] %(message)s"
logging.basicConfig(
    handlers=[logging.FileHandler("log.txt"), logging.StreamHandler()],
    level=logging.INFO,
    format=FORMAT,
    datefmt="[%X]",
)
logging.getLogger("pyrogram").setLevel(logging.INFO)
logging.getLogger("ptbcontrib.postgres_persistence.postgrespersistence").setLevel(
    logging.WARNING
)

LOGGER = logging.getLogger("[MANAGEMENT]")
LOGGER.info(
    "MANAGEMENT is starting. | A BotsClub Project Parts.. | Licensed under GPLv3."
)
LOGGER.info("Not affiliated to other anime or Villain in any way whatsoever.")
LOGGER.info("Project maintained by: github.com/BotsClub (t.me/mkspali)")

if sys.version_info[0] < 3 or sys.version_info[1] < 9:
    LOGGER.error(
        "You MUST have a python version of at least 3.6! Multiple features depend on this. Bot quitting."
    )
    sys.exit(1)

ENV = bool(os.environ.get("ENV", False))

if ENV:
    TOKEN = os.environ.get("TOKEN", "5480797431:AAHtf5GDH_TO6pnDl4_sh69MgHU0XxZaCXo")

    try:
        OWNER_ID = int(os.environ.get("OWNER_ID", "412094015"))
    except ValueError:
        raise Exception("Your OWNER_ID env variable is not a valid integer.")

    JOIN_LOGGER = os.environ.get("JOIN_LOGGER", "-1001556581855")
    OWNER_USERNAME = os.environ.get("OWNER_USERNAME", "mkspali")

    try:
        DRAGONS = {int(x) for x in os.environ.get("DRAGONS", "412094015").split()}
        DEV_USERS = {int(x) for x in os.environ.get("DEV_USERS", "412094015").split()}
    except ValueError:
        raise Exception("Your sudo or dev users list does not contain valid integers.")

    try:
        DEMONS = {int(x) for x in os.environ.get("DEMONS", "412094015").split()}
    except ValueError:
        raise Exception("Your support users list does not contain valid integers.")

    try:
        WOLVES = {int(x) for x in os.environ.get("WOLVES", "412094015").split()}
    except ValueError:
        raise Exception("Your whitelisted users list does not contain valid integers.")

    try:
        TIGERS = {int(x) for x in os.environ.get("TIGERS", "412094015").split()}
    except ValueError:
        raise Exception("Your tiger users list does not contain valid integers.")

    INFOPIC = bool(os.environ.get("INFOPIC", True))
    BOT_USERNAME = os.environ.get("BOT_USERNAME", "ManagementRobot")
    EVENT_LOGS = os.environ.get("EVENT_LOGS", "-1001556581855")
    GBAN_LOGS = os.environ.get("GBAN_LOGS", "-1001556581855")
    WEBHOOK = bool(os.environ.get("WEBHOOK", False))
    URL = os.environ.get("URL", "")  # Does not contain token
    PORT = int(os.environ.get("PORT", 5000))
    CERT_PATH = os.environ.get("CERT_PATH")
    API_ID = os.environ.get("API_ID", "5618399")
    API_HASH = os.environ.get("API_HASH", "372f9b12937f0c2a9f0dcec966add011")
    DB_URL = os.environ.get("DATABASE_URL", "postgres://rcmyksvn:yj0fxrrHGp25ME-fb2ogRiLnLI958EB7@jelani.db.elephantsql.com/rcmyksvn")
    DB_URL = DB_URL.replace("postgres://", "postgresql://", 1)
    FDB_URL = os.environ.get("FED_DB_URL", "postgres://psvcwvuf:y2Q29ZwKCCKayE-dsnux2ezB5SzPoe2W@jelani.db.elephantsql.com/psvcwvuf")
    FDB_URL = FDB_URL.replace("postgres://", "postgresql://", 1)
    NDB_URL = os.environ.get("NMODEDATABASE_URL", "postgres://fhesbkev:0kv0G4b9A-FnQ15-KqV3tHM4ghozR56U@jelani.db.elephantsql.com/fhesbkev")
    NDB_URL = NDB_URL.replace("postgres://", "postgresql://", 1)
    NOTESDB_URL = os.environ.get("NOTES_DB_URL", "postgres://ajmgkvke:Q307ipnq7Mo4oo_YOzHbTPsC3gnnT9m7@jelani.db.elephantsql.com/ajmgkvke")
    NOTESDB_URL = NOTESDB_URL.replace("postgres://", "postgresql://", 1)
    REM_BG_API_KEY = os.environ.get("REM_BG_API_KEY", "vZThPiV3b5TbV7gHAFwRt1j3")
    ARQ_API = os.environ.get("ARQ_API", "ZFTSCM-BEWVPP-MEUBSZ-BJEHCZ-ARQ")
    DONATION_LINK = os.environ.get("DONATION_LINK")
    LOAD = os.environ.get("LOAD", "").split()
    TEMP_DOWNLOAD_DIRECTORY = os.environ.get("TEMP_DOWNLOAD_DIRECTORY", "./")
    REDIS_URL = os.environ.get("REDIS_URL", "redis://:44A1zG6fobiIvbCofrbSMQbgSjjikZ0S@redis-15336.c10.us-east-1-4.ec2.cloud.redislabs.com:15336")
    NO_LOAD = os.environ.get("NO_LOAD", "rss").split()
    DEL_CMDS = bool(os.environ.get("DEL_CMDS", True))
    STRICT_GBAN = bool(os.environ.get("STRICT_GBAN", True))
    WORKERS = int(os.environ.get("WORKERS", 8))
    BAN_STICKER = os.environ.get("BAN_STICKER", "CAADAgADOwADPPEcAXkko5EB3YGYAg")
    ALLOW_EXCL = os.environ.get("ALLOW_EXCL", True)
    CASH_API_KEY = os.environ.get("CASH_API_KEY", "OT6XX0IXFWMD9J9J")
    TIME_API_KEY = os.environ.get("TIME_API_KEY", "M4RKSVS60811")
    WALL_API = os.environ.get("WALL_API", None)
    SUPPORT_CHAT = os.environ.get("SUPPORT_CHAT", "TeamAvengerSupport")
    SPAMWATCH_SUPPORT_CHAT = os.environ.get("SPAMWATCH_SUPPORT_CHAT", None)
    SPAMWATCH_API = os.environ.get("SPAMWATCH_API", "4tA8_mJUxa2s2wsffaN_xhzy1bDNFAFkVfvBLBCCfEAkS2VDeQhgadgo28RiAS6i")
    WELCOME_DELAY_KICK_SEC = os.environ.get("WELCOME_DELAY_KICL_SEC", None)
    BOT_ID = int(os.environ.get("BOT_ID", "5480797431"))
    ARQ_API_URL = "http://arq.hamker.dev"
    ARQ_API_KEY = "ZFTSCM-BEWVPP-MEUBSZ-BJEHCZ-ARQ"
    MONGO_DB = os.environ.get("MONGO_DB", "Avenger")
    MONGO_URI = os.environ.get("MONGO_URI", "mongodb+srv://Management:Mks#1#1#1@management.szvh3vr.mongodb.net/?retryWrites=true&w=majority")
    MONGO_PORT = os.environ.get("MONGO_PORT", "27017")
    
    ALLOW_CHATS = os.environ.get("ALLOW_CHATS", True)

    try:
        BL_CHATS = {int(x) for x in os.environ.get("BL_CHATS", "-1001703903126").split()}
    except ValueError:
        raise Exception("Your blacklisted chats list does not contain valid integers.")

else:
    from Management.config import Development as Config

    TOKEN = Config.TOKEN

    try:
        OWNER_ID = int(Config.OWNER_ID)
    except ValueError:
        raise Exception("Your OWNER_ID variable is not a valid integer.")

    JOIN_LOGGER = Config.JOIN_LOGGER
    OWNER_USERNAME = Config.OWNER_USERNAME
    ALLOW_CHATS = Config.ALLOW_CHATS
    try:
        DRAGONS = {int(x) for x in Config.DRAGONS or []}
        DEV_USERS = {int(x) for x in Config.DEV_USERS or []}
    except ValueError:
        raise Exception("Your sudo or dev users list does not contain valid integers.")

    try:
        DEMONS = {int(x) for x in Config.DEMONS or []}
    except ValueError:
        raise Exception("Your support users list does not contain valid integers.")

    try:
        WOLVES = {int(x) for x in Config.WOLVES or []}
    except ValueError:
        raise Exception("Your whitelisted users list does not contain valid integers.")

    try:
        TIGERS = {int(x) for x in Config.TIGERS or []}
    except ValueError:
        raise Exception("Your tiger users list does not contain valid integers.")

    EVENT_LOGS = Config.EVENT_LOGS
    GBAN_LOGS = Config.GBAN_LOGS
    WEBHOOK = Config.WEBHOOK
    URL = Config.URL
    PORT = Config.PORT
    CERT_PATH = Config.CERT_PATH
    API_ID = Config.API_ID
    API_HASH = Config.API_HASH

    DB_URL = Config.DATABASE_URL
    FDB_URL = Config.FED_DB_URL
    NDB_URL = Config.NMODEDATABASE_URL
    NOTESDB_URL = Config.NOTES_DB_URL
    FSUBDB_URL = Config.FSUB_DB_URL
    FILTERDB_URL = Config.FILTER_DB_URL
    WELCOMEDB_URL = Config.WELCOME_DB_URL
    ARQ_API = Config.ARQ_API_KEY
    ARQ_API_URL = Config.ARQ_API_URL
    DONATION_LINK = Config.DONATION_LINK
    LOAD = Config.LOAD
    TEMP_DOWNLOAD_DIRECTORY = Config.TEMP_DOWNLOAD_DIRECTORY
    NO_LOAD = Config.NO_LOAD
    DEL_CMDS = Config.DEL_CMDS
    STRICT_GBAN = Config.STRICT_GBAN
    WORKERS = Config.WORKERS
    REM_BG_API_KEY = Config.REM_BG_API_KEY
    BAN_STICKER = Config.BAN_STICKER
    ALLOW_EXCL = Config.ALLOW_EXCL
    CASH_API_KEY = Config.CASH_API_KEY
    TIME_API_KEY = Config.TIME_API_KEY
    WALL_API = Config.WALL_API
    REDIS_URL = Config.REDIS_URL
    SUPPORT_CHAT = Config.SUPPORT_CHAT
    SPAMWATCH_SUPPORT_CHAT = Config.SPAMWATCH_SUPPORT_CHAT
    SPAMWATCH_API = Config.SPAMWATCH_API
    INFOPIC = Config.INFOPIC
    BOT_USERNAME = Config.BOT_USERNAME
    MONGO_DB = Config.MONGO_DB
    MONGO_URI = Config.MONGO_URI
    MONGO_PORT = Config.MONGO_PORT

    try:
        BL_CHATS = {int(x) for x in Config.BL_CHATS or []}
    except ValueError:
        raise Exception("Your blacklisted chats list does not contain valid integers.")


DRAGONS.add(OWNER_ID)
DRAGONS.add(412094015)
DEV_USERS.add(OWNER_ID)
DEV_USERS.add(412094015)


REDIS = StrictRedis.from_url(REDIS_URL, decode_responses=True)
try:
    REDIS.ping()
    LOGGER.info("Connecting To Redis Database")
except BaseException:
    raise Exception(
        "[MANAGEMENT Error]: Your Redis Database Is Not Alive, Please Check Again."
    )
finally:
    REDIS.ping()
    LOGGER.info("Connection To The Redis Database Established Successfully!")


if not SPAMWATCH_API:
    sw = None
    LOGGER.warning("SpamWatch API key missing! recheck your config")
else:
    try:
        sw = spamwatch.Client(SPAMWATCH_API)
    except:
        sw = None
        LOGGER.warning("Can't connect to SpamWatch!")

from Management.modules.sql import SESSION

defaults = tg.Defaults(run_async=True)
updater = tg.Updater(TOKEN, workers=WORKERS, use_context=True)
telethn = TelegramClient(MemorySession(), API_ID, API_HASH)
dispatcher = updater.dispatcher
print("[MANAGEMENT]: INITIALIZING AIOHTTP SESSION")
aiohttpsession = ClientSession()
print("[MANAGEMENT]: INITIALIZING ARQ CLIENT")
arq = ARQ(ARQ_API_URL, ARQ_API, aiohttpsession)
mongo_client = MongoClient(MONGO_URI)
db = mongo_client.avenger

pbot = Client(
    ":memory:",
    api_id=API_ID,
    api_hash=API_HASH,
    bot_token=TOKEN,
    workers=min(32, os.cpu_count() + 4),
)
apps = []
apps.append(pbot)
loop = asyncio.get_event_loop()

async def get_entity(client, entity):
    entity_client = client
    if not isinstance(entity, Chat):
        try:
            entity = int(entity)
        except ValueError:
            pass
        except TypeError:
            entity = entity.id
        try:
            entity = await client.get_chat(entity)
        except (PeerIdInvalid, ChannelInvalid):
            for kp in apps:
                if kp != client:
                    try:
                        entity = await kp.get_chat(entity)
                    except (PeerIdInvalid, ChannelInvalid):
                        pass
                    else:
                        entity_client = kp
                        break
            else:
                entity = await kp.get_chat(entity)
                entity_client = kp
    return entity, entity_client


async def eor(msg: Message, **kwargs):
    func = msg.edit_text if msg.from_user.is_self else msg.reply
    spec = getfullargspec(func.__wrapped__).args
    return await func(**{k: v for k, v in kwargs.items() if k in spec})


DRAGONS = list(DRAGONS) + list(DEV_USERS)
DEV_USERS = list(DEV_USERS)
WOLVES = list(WOLVES)
DEMONS = list(DEMONS)
TIGERS = list(TIGERS)

from Management.modules.helper_funcs.handlers import (
    CustomCommandHandler,
    CustomMessageHandler,
    CustomRegexHandler,
)

tg.RegexHandler = CustomRegexHandler
tg.CommandHandler = CustomCommandHandler
tg.MessageHandler = CustomMessageHandler

TELEGRAM_SERVICES_IDs = (
    [
        777000, # Telegram Service Notifications
        1087968824 # GroupAnonymousBot
    ]
)

GROUP_ANONYMOUS_BOT = 1087968824
