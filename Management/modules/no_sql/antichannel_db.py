from Management.modules.no_sql import get_collection

ANTI_CHANNEL = get_collection("ANTI_CHANNEL")

def antichannel_status(chat_id):
    is_achannel = ANTI_CHANNEL.find_one({"chat_id": chat_id})
    if not is_achannel:
        return False
    else:
        return True

def enable_antichannel(chat_id):
    is_achannel = antichannel_status(chat_id)
    if is_achannel:
        return
    else:
        return ANTI_CHANNEL.insert_one({"chat_id": chat_id})

def disable_antichannel(chat_id):
    is_achannel = antichannel_status(chat_id)
    if not is_achannel:
        return
    else:
        return ANTI_CHANNEL.delete_one({"chat_id": chat_id})
