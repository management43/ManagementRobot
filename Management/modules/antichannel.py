from telegram.ext.filters import Filters
from Management.modules.helper_funcs.decorators import managementcmd, managementmsg
from telegram import Update, message
from telegram.ext import CallbackContext
from ..modules.helper_funcs.anonymous import user_admin, AdminPerms
import html
from Management.modules.no_sql.antichannel_db import antichannel_status, disable_antichannel, enable_antichannel


@managementcmd(command="antichannel", group=100)
@user_admin(AdminPerms.CAN_RESTRICT_MEMBERS)
def set_antichannel(update: Update, context: CallbackContext):
    message = update.effective_message
    chat = update.effective_chat
    if chat.type == "private":
        return
    args = context.args
    if len(args) > 0:
        s = args[0].lower()
        if s in ["yes", "on"]:
            enable_antichannel(chat.id)
            message.reply_html("✅ Anti-Channel <b>Enabled</b> in {}".format(html.escape(chat.title)))
        elif s in ["off", "no"]:
            disable_antichannel(chat.id)
            message.reply_html("❌ Anti-Channel <b>Disabled</b> in {}".format(html.escape(chat.title)))
        else:
            message.reply_text("⚠️ Wrong keyword use /antichannel on/off instead of {}".format(s))
        return
    message.reply_html(
        "Anti-Channel setting is currently {} in {}".format(antichannel_status(chat.id), html.escape(chat.title)))


@managementmsg(Filters.chat_type.groups, group=110)
def eliminate_channel(update: Update, context: CallbackContext):
    message = update.effective_message
    chat = update.effective_chat
    bot = context.bot
    if not antichannel_status(chat.id):
        return
    if message.sender_chat and message.sender_chat.type == "channel" and not message.is_automatic_forward:
        message.delete()
        sender_chat = message.sender_chat
        bot.ban_chat_sender_chat(sender_chat_id=sender_chat.id, chat_id=chat.id)


__help__ = """
*ANTI-CHANNEL MODULE*
/antichannel on/yes : Turn On Antichannel Function
/antichannel off/no : Turn Off Antichannel Function
/antichannel : For check the status of antichannel function
"""

__mod_name__ = "A-ᴄʜᴀɴɴᴇʟ"
